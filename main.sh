#!/usr/bin/env bash

export download_folder="./downloaded_files"

echo >&2 "[INFO] Creating or touching download folder with name \"$download_folder\""
mkdir -p "$download_folder"

declare -a urls
urls=(
    # Example urls
    https://wkhtmltopdf.org
    https://es.wikipedia.org/wiki/Toyotomi_Hideyoshi
    https://es.wikipedia.org/wiki/Yi_Sun-sin
)

is_wkhtmltopdf_installed() {
    dpkg -s wkhtmltopdf &> /dev/null
    return $?
}

install() {
    echo >&2 "[INFO] Installing wkhtmltopdf"
    sudo apt install -y wkhtmltopdf
}

download_and_convert_pages() {
    echo >&2 "[INFO] Downloading and converting pages"
    (IFS=$'\n'; echo "${urls[*]}") | xargs -P0 -I _url bash -c '
        url=_url
        filename="${url##*/}"
        pdf_name="${filename}.pdf"
        wkhtmltopdf -q "$url" "$download_folder/$pdf_name"
    '
}

main() {
    is_wkhtmltopdf_installed || {
        echo >&2 "[INFO] wkhtmltopdf is not installed. Installing..."
        install  
    } 
    download_and_convert_pages
}

main