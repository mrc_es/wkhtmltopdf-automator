# Installador y automatizador de wkhtmltopdf

## Descripcion

Lo que dice el titulo.

Este script se probó con satisfacción en Ubuntu, así que supongo que para las distros Debian-based es aplicable.

## Instrucciones de instalación

1. Crear una carpeta vacía dedicada a este script y su resultado
2. Descargar o copiar script en un archivo llamado "main.sh" en la carpeta creada
3. Abrir la terminal y posicionarse en la carpeta dedicada, ejemplo, `cd /mi/carpeta/dedicada`
4. Ejecutar en la consola `chmod u+x main.sh` (esto solo se hace una vez)
5. Abrir el archivo "main.sh" y editar la lista de urls para poner las urls deseadas, luego guardarlo, ejemplo:
```bash
urls=(
    nueva url 1
    nueva url 2
)
```
6. Ejecutar en la consola `./main.sh`
7. El resultado debe de estar dentro de una carpeta llamada "downloaded_files". Esta carpeta se puede cambiar de nombre y direccion desde el archivo main.sh en la variable `download_folder`
